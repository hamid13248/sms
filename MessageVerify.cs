﻿using GlobalClass;
using System;
using System.Collections.Generic;
using System.Text;

namespace SMS
{
    public class MessageVerify:Json
    {

        /// <summary>
        /// // شماره گیرنده پیام(برای مثال 09122222222)   اجباری
        /// </summary>
        public string receptor { get; set; }
        /// <summary>
        /// //type    Body Integer برای ارسال پیام متنی type=1 و برای ارسال پیام صوتی type = 2 قرار دهید اجباری
        /// </summary>
        public int type { get; set; } = 1;
        /// <summary>
        /// //template    Body string عنوان قالبی که در پنل خود ایجاد کرده ایید   اجباری
        /// </summary>
        public string template { get; set; }
        /// <summary>
        /// //param1  Body string مقادیری که از سمت شما وارد می شود ، وارد کردن حداقل 1 مورد اجباریست اجباری
        /// </summary>
        public string param1 { get; set; }
        /// <summary>
        /// ///param2  Body string مقادیری که از سمت شما وارد می شود ، وارد کردن حداقل 1 مورد اجباریست اختیاری
        /// </summary>
        public string param2 { get; set; }
        /// <summary>
        /// //param3  Body string مقادیری که از سمت شما وارد می شود ، وارد کردن حداقل 1 مورد اجباریست اختیاری
        /// </summary>
        public string param3 { get; set; }
    }
}






