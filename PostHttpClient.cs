﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SMS
{
    class PostHttpClient
    {
        static HttpClient Server = new HttpClient();
        internal static async Task<ResutltWS> SendAsync(string ApiUrl, string message,string apikey)
        {
            //Security
            Server.DefaultRequestHeaders.Clear();
            //Option
            TimeSpan TimeOut = new TimeSpan(0, 0, 20);//تایم اوت بعد این زمان بپرد بیرون اگر درخواست نیومد
            
            if ((Server.Timeout.Seconds != TimeOut.Seconds))
            {
                Server.Timeout = TimeOut;// new TimeSpan(0, 0, 10);//تایم اوت بعد این زمان بپرد بیرون اگر درخواست نیومد
            }
            ResutltWS Result = new ResutltWS() { Status = System.Net.HttpStatusCode.Unused };
            var _Content = new StringContent(message, Encoding.UTF8, "application/json");
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.Unused);

            Server.DefaultRequestHeaders.Add("apikey", apikey);

            try
            {//همیشه  این بخش فراخوانی میشود
                response = await Server.PostAsync(ApiUrl, _Content);// "Data/Run"
            }
            catch (Exception)
            {
                //todo 1006
                Result = new ResutltWS() { Status = System.Net.HttpStatusCode.InternalServerError, Message = "MessageWS1006" };//$$$ MessageText
                return Result;
            }
          

            switch (response.StatusCode)
            {
                case System.Net.HttpStatusCode.OK:
                    var DataJson = await response.Content.ReadAsStringAsync();


                    Result = new ResutltWS() { Status = System.Net.HttpStatusCode.OK, Content = DataJson };
                    return Result;
                    break;
                case System.Net.HttpStatusCode.NoContent://todo 1007
                    Result = new ResutltWS() { Status = response.StatusCode, Message = "MessageWS1007" };//$$$ MessageText
                    break;
                case System.Net.HttpStatusCode.NotFound://todo 1008
                    Result = new ResutltWS() { Status = response.StatusCode, Message = "MessageWS1008" };//$$$ MessageText
                    break;
                case System.Net.HttpStatusCode.BadRequest://todo 1009
                    Result = new ResutltWS() { Status = response.StatusCode, Message = "MessageWS1009" };//$$$ MessageText
                    break;
                case System.Net.HttpStatusCode.Unauthorized:
                    //$$$
                    break;
                case System.Net.HttpStatusCode.RequestTimeout://todo 1010
                    Result = new ResutltWS() { Status = response.StatusCode, Message = "MessageWS1010" };//$$$ MessageText
                    break;
                case System.Net.HttpStatusCode.InternalServerError:
                default://todo 1011
                    //اگر به اینجا برسد یعنی خطایی آمده است
                    Result = new ResutltWS() { Status = response.StatusCode, Message = "MessageWS1011" };//$$$ MessageText
                    break;
            }
            return Result;
        }
    }
}